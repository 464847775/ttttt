import myAxios from "./http";

export const getJournal = () => {
  return myAxios.get("./journal");
};
