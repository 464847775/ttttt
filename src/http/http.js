import axios from "axios";
const myAxios = axios.create({
  baseURL: import.meta.env.VITE_BASE_URL + "/api",
  timeout: 3000,
});

myAxios.interceptors.request.use(
  (config) => {
    config.headers = {
      ...config.headers,
      "Content-Type": "application/json",
    };
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

myAxios.interceptors.response.use(
  (res) => {
    console.log(res.data);
    return res.data;
  },
  (error) => {
    console.log("error=", error);
    return Promise.reject(error);
  }
);

export const get = (url, requestable) => {
  return myAxios.get(url, {
    params: requestable,
  });
};

export const post = (url, requestable) => {
  return myAxios.post(url, requestable);
};

export default myAxios;
