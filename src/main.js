import { createApp } from "vue";
import App from "./App.vue";

import "default-passive-events";
import "./assets/main.css";
import "./utils/daily-task";
import "./utils/hour-task";
import "./utils/utils";

createApp(App).mount("#app");
console.log("env=", import.meta.env);
