import { ref } from "vue";

//获取当前时间
export const getFormatDateTime = () => {
  var date = new Date();
  var year = date.getFullYear();
  var month = date.getMonth() + 1;
  var day = date.getDate();
  var hour = date.getHours();
  var minute = date.getMinutes();
  var second = date.getSeconds();
  return [year, "-", month, "-", day, " ", hour, ":", minute, ":", second].join(
    ""
  );
};

//获取距离24小时的时间间隔
export const getTimeTo24 = () => {
  var date = new Date();
  var hour = date.getHours();
  var minute = date.getMinutes();
  var second = date.getSeconds();

  var time = 24 * 60 * 60 - (hour * 60 * 60 + minute * 60 + second);
  return time;
};

//获取距离下一小时的时间间隔
export const getTimeToNextHour = () => {
  var date = new Date();
  var minute = date.getMinutes();
  var second = date.getSeconds();

  var time = 60 * 60 - (minute * 60 + second);
  return time;
};

//随机整数
export const randomInt = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};
console.log(getFormatDateTime());
console.log(getTimeTo24());
console.log(getTimeToNextHour());

//定时器任务
export const simulateInterval = (callback, interval) => {
  let timerId = ref(null);
  function fn() {
    console.log("执行定时器代码", timerId.value);
    callback();
    const prevTimmerId = timerId.value;
    timerId.value = setTimeout(fn, interval);
    clearTimeout(prevTimmerId);
    console.log("timer=", timerId);
  }
  timerId.value = setTimeout(fn, interval);
  return timerId;
};
