import { getFormatDateTime, simulateInterval } from "./utils";
// function hourlyRefresh(callback) {
//   var date, hour, minute, second, readyHour;
//   setInterval(function () {
//     date = new Date();
//     hour = date.getHours();
//     minute = date.getMinutes();
//     second = date.getSeconds();
//     console.log("当前时间：" + hour + ":" + minute + ":" + second);
//     if (minute === 0) {
//       if (!readyHour) {
//         readyHour = true;
//         callback(hour);
//       }
//     } else {
//       readyHour = false;
//     }
//   }, 10000);
// }

function hourlyRefresh(callback) {
  var date, hour, minute, second, readyHour;
  date = new Date();
  hour = date.getHours();
  minute = date.getMinutes();
  second = date.getSeconds();

  var time = minute * 60 + second;
  var left_time = 0;

  if (time > 0) {
    left_time = 60 * 60 - time;
  }

  console.log("距离执行小时任务还有", left_time);
  setTimeout(() => {
    console.log("执行小时任务", getFormatDateTime());
    simulateInterval(() => {
      console.log("执行小时任务", getFormatDateTime());
    }, 60 * 60 * 1000);
  }, left_time * 1000);

  setInterval(function () {
    date = new Date();
    hour = date.getHours();
    minute = date.getMinutes();
    second = date.getSeconds();
    console.log("当前时间：" + hour + ":" + minute + ":" + second);
    if (minute === 0) {
      if (!readyHour) {
        readyHour = true;
        callback(hour);
      }
    } else {
      readyHour = false;
    }
  }, 10000);
}

// hourlyRefresh(() => {
//   console.log("整点了", getFormatDateTime());
// });
