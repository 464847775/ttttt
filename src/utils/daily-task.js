import { getFormatDateTime } from "./utils";

//每日任务
export const config = {
  time: "00:00:00", // 每天几点执行
  interval: 1, // 隔几天执行一次
  runNow: 1, // 是否立即执行
  intervalTimer: "",
  timeOutTimer: "",
};

export const startTaskByTimer = () => {
  if (config.runNow) {
    // 如果配置了立刻运行则立刻运行任务函数
    startTask();
  }
  // 获取下次要执行的时间，如果执行时间已经过了今天，就让把执行时间设到明天的按时执行的时间
  var nowTime = new Date().getTime();
  var timePoint = config.time.split(":").map((i) => parseInt(i));

  var recent = new Date().setHours(...timePoint); // 获取执行时间的时间戳

  if (recent <= nowTime) {
    recent += 24 * 60 * 60 * 1000;
  }
  // 未来程序执行的时间减去现在的时间，就是程序要多少秒之后执行
  var doRunTime = recent - nowTime;
  console.log("距离执行24小时任务还有", doRunTime / 1000);
  if (config.timeOutTimer) clearTimeout(config.timeOutTimer);
  config.timeOutTimer = setTimeout(setTimer, doRunTime);
};

const setTimer = () => {
  console.log("进入定时器");
  //配置后的第一天12点执行
  startTask();
  // 每隔多少天再执行一次
  var intTime = config.interval * 24 * 60 * 60 * 1000;
  if (config.intervalTimer) clearInterval(config.intervalTimer);
  config.intervalTimer = setInterval(startTask, intTime);
};

const startTask = () => {
  console.log("执行24小时定时任务", getFormatDateTime());
};
// console.log(
//   '关闭任务定时器',
//    config.intervalTimer
// )
// clearInterval( config.intervalTimer)
// console.log('清除定时器timeout',  config.timeOutTimer)
// clearTimeout( config.timeOutTimer)

// startTaskByTimer();
